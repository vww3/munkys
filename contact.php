<?php

$category = get_cat_ID('contact');

$query = get_posts([
    'post_type' => 'post',
    'category' => $category,
]);
$post = $query[0];

if($post != null):
?>
<section id="Contact" class="section container-fluid d-flex align-items-center pt-5"
<?php munkysband_get_parallax_background_by_slug("contact-bg"); ?>
>
    <div class="row justify-content-center col-12">
      <h2
       class="col-12 text-center"
       data-aos="fade-up"
       data-aos-delay="200"
       ><?php echo $post->post_title; ?></h2>
      <hr class="col-4"
      data-aos="fade-up"
      data-aos-delay="300"
      >

    <div class="row justify-content-center text-center"
    data-aos="fade-up"
    data-aos-delay="400"
    >
      <div
      class="col-11">
          <?php echo $post->post_content; ?>
      </div>

      <?php $reseaux = wp_get_nav_menu_items('reseaux'); ?>
    <ul class="row col-10 col-sm-8 col-md-6 col-lg-5 col-xl-4">
      <?php
      foreach ($reseaux as $reseau) {
          $reseauName = $reseau->title;

          $image = get_attachment_url_by_slug($reseauName);
          ?>
          <li class="col"><img class="" src="<?php echo $image; ?>" alt="<?php echo $reseauName; ?>"><a href="<?php echo $reseau->url ?>" target="_blank"></a></li>
          <?php

      }
    ?>
    </ul>
  </div>
  </div>

</section>
  <footer id="colophon" class="site-footer container-fluid">
    <div class="site-info">
      <small>
        <p class="text-center p-2 m-0">
        Site réalisé par
        <a href="http://www.yannis-guillemote.fr/">Yannis Guillemote</a>
      </p>
    </small>

    </div><!-- .site-info -->
  </footer><!-- #colophon -->

<?php endif; ?>
