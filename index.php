<?php
 //Thème de base utilisé : Underscores
 //Appel du header wp
 get_header();

 //Appel des différentes parties du thème

 //D'abord les parties fixed et les éléments masqués
 get_template_part("sidebar");
 get_template_part("lightbox");

 //Les différentes parties visible du site
 get_template_part("homepage");
 get_template_part("navbar");
 get_template_part("groupe");
 get_template_part("programmation");
 get_template_part("videos");
 get_template_part("photos");
 get_template_part("contact");

 //Le footer wp
 get_footer();

 //Pour les fonctions php personnalisées, aller dans inc/template-functions.php
 //Pour la liste des scripts, les feuilles de style chargées, la déclaration des customs posts, aller dans functions.php

?>
