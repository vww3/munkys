<!-- barre de navigation fixed.
Gérée par navbar.js -->
<nav id="navbar_top" class="site-navigation navbar navbar-expand-lg navbar-dark">
 <div class="container-fluid">
  <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#main_nav">
      <span class="navbar-toggler-icon"></span>
    </button>

    <?php

    wp_nav_menu(
           array(
             'theme_location' => 'menu-1',
             'menu_id'        => 'primary-menu',
             'container_class' => 'collapse navbar-collapse',
             'container_id' => 'main_nav',
              'menu_class' => 'navbar-nav ms-auto p-0',
              'list_item_class'  => 'nav-item col-auto',
              'link_class'   => 'nav-link'
           )
         );

     ?>
 <!-- navbar-collapse.// -->
 </div> <!-- container-fluid.// -->
</nav>
