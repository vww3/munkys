<?php
//Affiche les réseaux sociaux en sidebar sur grand écran
if ( ! is_active_sidebar( 'sidebar-1' ) ) {
	return;
}
?>

<aside id="reseaux" class="widget-area"

data-aos="fade-right"
data-aos-delay="200"
>
    <?php
munkysband_get_socials();
					 ?>
</aside><!-- #secondary -->
