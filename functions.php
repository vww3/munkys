<?php
/**
* munkysband functions and definitions
*
* @link https://developer.wordpress.org/themes/basics/theme-functions/
*
* @package munkysband
*/

if ( ! defined( '_S_VERSION' ) ) {
	// Replace the version number of the theme on each release.
	define( '_S_VERSION', '1.0.0' );
}

if ( ! function_exists( 'munkysband_setup' ) ) :
	/**
	* Sets up theme defaults and registers support for various WordPress features.
	*
	* Note that this function is hooked into the after_setup_theme hook, which
	* runs before the init hook. The init hook is too late for some features, such
	* as indicating support for post thumbnails.
	*/
	function munkysband_setup() {
		/*
		* Make theme available for translation.
		* Translations can be filed in the /languages/ directory.
		* If you're building a theme based on munkysband, use a find and replace
		* to change 'munkysband' to the name of your theme in all the template files.
		*/
		load_theme_textdomain( 'munkysband', get_template_directory() . '/languages' );

		// Add default posts and comments RSS feed links to head.
		add_theme_support( 'automatic-feed-links' );

		/*
		* Let WordPress manage the document title.
		* By adding theme support, we declare that this theme does not use a
		* hard-coded <title> tag in the document head, and expect WordPress to
		* provide it for us.
		*/
		add_theme_support( 'title-tag' );

		/*
		* Enable support for Post Thumbnails on posts and pages.
		*
		* @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
		*/
		add_theme_support( 'post-thumbnails' );

		// This theme uses wp_nav_menu() in one location.
		register_nav_menus(
			array(
				'menu-1' => esc_html__( 'Primary', 'munkysband' ),
			)
		);

		/*
		* Switch default core markup for search form, comment form, and comments
		* to output valid HTML5.
		*/
		add_theme_support(
			'html5',
			array(
				'search-form',
				'comment-form',
				'comment-list',
				'gallery',
				'caption',
				'style',
				'script',
			)
		);

		// Set up the WordPress core custom background feature.
		add_theme_support(
			'custom-background',
			apply_filters(
				'munkysband_custom_background_args',
				array(
					'default-color' => 'ffffff',
					'default-image' => '',
				)
				)
			);

			/*Menu classes*/
			add_filter( 'nav_menu_link_attributes', 'add_menu_link_class', 1, 3 );
			add_filter('nav_menu_css_class', 'add_menu_list_item_class', 1, 3);

			function add_menu_link_class( $atts, $item, $args ) {
				if (property_exists($args, 'link_class')) {
					$atts['class'] = $args->link_class;
				}
				return $atts;
			}

			function add_menu_list_item_class($classes, $item, $args) {
				if (property_exists($args, 'list_item_class')) {
					$classes[] = $args->list_item_class;
				}
				return $classes;
			}


			// Add theme support for selective refresh for widgets.
			add_theme_support( 'customize-selective-refresh-widgets' );

			/**
			* Add support for core custom logo.
			*
			* @link https://codex.wordpress.org/Theme_Logo
			*/
			add_theme_support(
				'custom-logo',
				array(
					'height'      => 250,
					'width'       => 250,
					'flex-width'  => true,
					'flex-height' => true,
					'unlink-homepage-logo' => true,
					'class', 'col-12',
				)
			);
		}
	endif;
	add_action( 'after_setup_theme', 'munkysband_setup' );

	/**
	* Set the content width in pixels, based on the theme's design and stylesheet.
	*
	* Priority 0 to make it available to lower priority callbacks.
	*
	* @global int $content_width
	*/
	function munkysband_content_width() {
		$GLOBALS['content_width'] = apply_filters( 'munkysband_content_width', 640 );
	}
	add_action( 'after_setup_theme', 'munkysband_content_width', 0 );

	/**
	* Register widget area.
	*
	* @link https://developer.wordpress.org/themes/functionality/sidebars/#registering-a-sidebar
	*/

	//Custom Posts videos et concerts

	function munkysband_videos_post_type() {
		register_taxonomy_for_object_type( 'category', 'videos' ); // Register Taxonomies for Category
		register_taxonomy_for_object_type( 'post_tag', 'videos' );
		register_post_type('videos', // Register Custom Post Type
		array(
			'labels'       => array(
				'name'               => esc_html( 'Vidéos', 'munkysband' ), // Rename these to suit
				'singular_name'      => esc_html( 'Vidéos', 'munkysband' ),
				'add_new'            => esc_html( 'Ajouter', 'munkysband' ),
				'add_new_item'       => esc_html( 'Ajouter une vidéo', 'munkysband' ),
				'edit'               => esc_html( 'Editer', 'munkysband' ),
				'edit_item'          => esc_html( 'Editer la vidéo', 'munkysband' ),
				'new_item'           => esc_html( 'Nouvelle vidéo', 'munkysband' ),
				'view'               => esc_html( 'Voir', 'munkysband' ),
				'view_item'          => esc_html( 'Voir la vidéo', 'munkysband' ),
				'search_items'       => esc_html( 'Chercher une vidéo', 'munkysband' ),
				'not_found'          => esc_html( 'Aucune vidéo trouvée', 'munkysband' ),
				'not_found_in_trash' => esc_html( 'Aucune vidéo dans la corbeille', 'munkysband' ),
			),
			'public'       => true,
			'hierarchical' => true, // Allows your posts to behave like Hierarchy Pages
			'has_archive'  => true,
			'supports'     => array(
				'title',
				'custom-fields',

				//'url'
				//'excerpt',
				//'thumbnail'
			), // Go to Dashboard Custom HTML5 Blank post for supports
			'can_export'   => true, // Allows export in Tools > Export
			'taxonomies'   => array(
				'post_tag',
				//'category'
			) // Add Category and Post Tags support
		) );
	}

	add_action( 'init', 'munkysband_videos_post_type' ); // Add our HTML5 Blank Custom Post Type


	function munkysband_concerts_post_type() {
		register_taxonomy_for_object_type( 'category', 'concerts' ); // Register Taxonomies for Category
		register_taxonomy_for_object_type( 'post_tag', 'concerts' );
		register_post_type('concerts', // Register Custom Post Type
		array(
			'labels'       => array(
				'name'               => esc_html( 'Concerts', 'munkysband' ), // Rename these to suit
				'singular_name'      => esc_html( 'Concerts', 'munkysband' ),
				'add_new'            => esc_html( 'Ajouter', 'munkysband' ),
				'add_new_item'       => esc_html( 'Ajouter un concert', 'munkysband' ),
				'edit'               => esc_html( 'Editer', 'munkysband' ),
				'edit_item'          => esc_html( 'Editer le concert', 'munkysband' ),
				'new_item'           => esc_html( 'Nouveau concert', 'munkysband' ),
				'view'               => esc_html( 'Voir', 'munkysband' ),
				'view_item'          => esc_html( 'Voir le concert', 'munkysband' ),
				'search_items'       => esc_html( 'Chercher un concert', 'munkysband' ),
				'not_found'          => esc_html( 'Aucun concert trouvé', 'munkysband' ),
				'not_found_in_trash' => esc_html( 'Aucun concert dans la corbeille', 'munkysband' ),
			),
			'public'       => true,
			'hierarchical' => true, // Allows your posts to behave like Hierarchy Pages
			'has_archive'  => true,
			'rewrite' => array('slug','concerts'),
			'supports'     => array(
				'title',
				'editor',
				'custom-fields',
				'excerpt',
				//'thumbnail'
			), // Go to Dashboard Custom HTML5 Blank post for supports
			'can_export'   => true, // Allows export in Tools > Export
			'taxonomies'   => array(
				//'post_tag',
				//'category'
			) // Add Category and Post Tags support
		) );
	}

	add_action( 'init', 'munkysband_concerts_post_type' );
	function remove_admin_login_header() {
		remove_action('wp_head', '_admin_bar_bump_cb');
	}
	add_action('get_header', 'remove_admin_login_header');

	add_action( 'widgets_init', 'munkysband_widgets_init' );


	function munkysband_widgets_init() {
		register_sidebar(
			array(
				'name'          => esc_html__( 'Sidebar', 'munkysband' ),
				'id'            => 'sidebar-1',
				'description'   => esc_html__( 'Add widgets here.', 'munkysband' ),
				'before_widget' => '<section id="%1$s" class="widget %2$s">',
				'after_widget'  => '</section>',
				'before_title'  => '<h2 class="widget-title">',
				'after_title'   => '</h2>',
			)
		);
	}
	add_action( 'widgets_init', 'munkysband_widgets_init' );

	/**
	* Enqueue scripts and styles.
	*/
	function munkysband_scripts() {

		wp_enqueue_style( 'bootstrap-style', 'https://cdn.jsdelivr.net/npm/bootstrap@5.0.1/dist/css/bootstrap.min.css', _S_VERSION );
		wp_enqueue_style( 'lightbox-style',  get_template_directory_uri() .'/css/lightbox.css', _S_VERSION );

		//Styles
		wp_enqueue_style( 'munkysband-style', get_stylesheet_uri(), array(), _S_VERSION );
		wp_style_add_data( 'munkysband-style', 'rtl', 'replace' );

		//Scripts
		wp_enqueue_script( 'jQuery', 'https://code.jquery.com/jquery-latest.min.js', array(), _S_VERSION, true );
		wp_enqueue_script( 'Popper', 'https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js', array(), _S_VERSION, true );
		wp_enqueue_script( 'Bootstrap', 'https://cdn.jsdelivr.net/npm/bootstrap@5.0.1/dist/js/bootstrap.bundle.min.js', array(), _S_VERSION, true );

		//Instagram feed
		wp_enqueue_script( 'fa_icons', "https://use.fontawesome.com/4ba3e129fd.js", array(), _S_VERSION );
		wp_enqueue_script( 'instafeed-token', 'https://ig.instant-tokens.com/users/1314c9ff-370b-4d93-9f17-aaead0366911/instagram/17841404236331962/token.js?userSecret=1yup19md96ub6ixrk0vunu', array(), _S_VERSION, true );
		wp_enqueue_script('instafeed', 'https://cdn.jsdelivr.net/gh/stevenschobert/instafeed.js@2.0.0rc1/src/instafeed.min.js', array(), _S_VERSION, true );
		wp_enqueue_script( 'instafeed_loader', get_template_directory_uri() .'/js/instafeed.js', array(), _S_VERSION, true );

		wp_enqueue_script( 'parallax', 'https://cdn.jsdelivr.net/parallax.js/1.4.2/parallax.min.js', array(), _S_VERSION, true );
		
		wp_enqueue_script('lightbox', get_template_directory_uri() .'/js/lightbox.js', array(), _S_VERSION, true );
		wp_enqueue_script('videoplayer', get_template_directory_uri() .'/js/videoplayer.js', array(), _S_VERSION, true );
		wp_enqueue_script('main', get_template_directory_uri() .'/js/main.js', array(), _S_VERSION, true );
		wp_enqueue_script('navbar', get_template_directory_uri() .'/js/navbar.js', array(), _S_VERSION, true );

		if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
			wp_enqueue_script( 'comment-reply' );
		}
	}
	add_action( 'wp_enqueue_scripts', 'munkysband_scripts' );

	/**
	* Implement the Custom Header feature.
	*/
	require get_template_directory() . '/inc/custom-header.php';

	/**
	* Custom template tags for this theme.
	*/
	require get_template_directory() . '/inc/template-tags.php';

	/**
	* Functions which enhance the theme by hooking into WordPress.
	*/
	require get_template_directory() . '/inc/template-functions.php';

	/**
	* Customizer additions.
	*/
	require get_template_directory() . '/inc/customizer.php';

	/**
	* Load Jetpack compatibility file.
	*/
	if ( defined( 'JETPACK__VERSION' ) ) {
		require get_template_directory() . '/inc/jetpack.php';
	}
