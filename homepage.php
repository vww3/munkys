<!--
Correspond à la première section du site,
affiche le logo et la description du groupe,
Ainsi qu'un menu
-->
<section
id="Home"
class="site-header shadow section d-flex align-items-center container-fluid parallax-window"
<?php  munkysband_get_parallax_background_by_slug('home-bg'); ?>
 >
  <div class="site-branding row justify-content-center">
    <?php
    $custom_logo_id = get_theme_mod( 'custom_logo' );
    $image = wp_get_attachment_image_src( $custom_logo_id , 'full' );
    ?>
    <div class="col-11 col-md-6">
      <img
      data-aos="fade-up"
      src="<?php echo $image['0']; ?>" alt="">
    </div>
    <?php
    $munkysband_description = get_bloginfo( 'description', 'display' );
    if ( $munkysband_description || is_customize_preview() ) :
      ?>
      <div class="row col-12 justify-content-center">
          <h1
          data-aos="fade-up"
          data-aos-delay="200"
          class="site-description text-center"><?php echo $munkysband_description; // phpcs:ignore WordPress.Security.EscapeOutput.OutputNotEscaped ?></h1>
      </div>
    <?php endif; ?>

    <nav
    class="row col-12 justify-content-center"
    data-aos="fade-up"
    data-aos-delay="300"
    >
      <hr class="col-4">
      <?php
          wp_nav_menu(
             array(
               'theme_location' => 'menu-1',
               'menu_id'        => 'primary-menu',
                'menu_class' => 'p-0 text-center row col-12 justify-content-center',
                'list_item_class'  => 'nav-item col-auto',
                'link_class'   => 'nav-link'
             )
            );

            ?>
    </nav>
  </div><!-- .site-branding -->


</section><!-- #masthead -->
