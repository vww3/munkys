<!-- Section dédiée à l'affichage des concerts à venir -->
<?php
//Récupération de tous les concerts à venir, voir template-functions.php
$posts = munkysband_get_concerts();
//var_dump($posts);

//N'affiche la section que si il y a au moins un concert à venir
if (count($posts) > 0) :
  ?>
  <section id="Programmation" class="container-fluid section" <?php munkysband_get_parallax_background_by_slug('programmation-bg'); ?>>
    <div class="justify-content-center col-12 row reveal pt-4">
      <h2
      class="col-12 text-center"
      data-aos="fade-up"
      >
      Programmation</h2>
      <hr
      class="col-4"
      data-aos="fade-up"
      data-aos-delay="200"
      >
    </div>
    <div class="programmation-list row justify-content-center d-flex align-content-around flex-wrap">
      <ul
      class="row align-self-center justify-content-center col-10 pb-4 col-lg-8"
      data-aos="zoom-out-up"
      data-aos-delay="300"
      >
      <?php
      //Affichage des concerts
      $i = 0;
      foreach ($posts as $post):
        ?>

        <li class="col-12 col-lg-4 col-xl-3 m-2 reveal text-center p-3">
          <a href="<?php echo get_post_permalink($post->ID);?>">
            <h3 class="h4 col"><?php echo $post->post_title; ?></h3>

            <div class=" d-none d-lg-block">
              <?php  echo $post->post_excerpt; ?>
            </div>
            <?php
            $meta = get_post_meta($post->ID);
            $date = $meta['date'][0];
            ?>
            <p class="m-0"><?php echo $date; ?></p>
          </a>
        </li>

        <?php
        $i++;
      endforeach; ?>
    </ul>
  </div>


</section>
<?php
endif; ?>
