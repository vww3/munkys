<!--
élément de liste de custom post de type vidéos
Appelé dans videos.php
-->
<?php $custom = get_post_custom();
$url = $custom['video'][0];
$id = munkysband_get_video_id($url);
?>

<li class="col-6 col-md-4 col-lg-3">
    <figure class="card-item" data-id="<?php echo $id; ?>">
      <img class="card-img" src="http://img.youtube.com/vi/<?php echo $id; ?>/sddefault.jpg" alt="<?php echo $id; ?>">
      <figcaption class="p-5 d-flex flex-column justify-content-center">
            <h5 class="text-center">
              <?php the_title();  ?>
            </h5>
      </figcaption>
    </figure>

</li>
