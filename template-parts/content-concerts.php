<?php
/**
 * Template part for displaying posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package munkysband
 */

?>


  <article id="post-<?php the_ID(); ?>" <?php post_class("row min-vh-100 justify-content-center"); ?>>
    <div class="col-12 col-lg-6 text-center text-lg-start single-concert-wrapper mb-3 px-4">
      <div class="row justify-content-start mt-4 ms-2">
            <button
            data-aos="fade-left"
            data-aos-delay="200" class="back-button p-0" type="button" name="button">
                <a class="d-block w-100 h-100" href="<?php bloginfo('wpurl'); ?>">
              <i class="fa fa-angle-left fa-2x" aria-hidden="true"></i>
                  </a>
            </button>

        </div>

        <div class="content row justify-content-center mt-4">

                <header class="entry-header row col-12">
                  <?php
                    the_title( '<h1
                    data-aos="fade-up"
                    class="entry-title col-12">', '</h1>' );
                    ?>

                    <hr
                    data-aos="fade-up"
                    data-aos-delay="200">
                </header><!-- .entry-header -->

                <?php munkysband_post_thumbnail(); ?>

                <div class="entry-content"
                    data-aos="fade-up"
                    data-aos-delay="400">
                  <?php
                  the_content(
                    sprintf(
                      wp_kses(
                        /* translators: %s: Name of current post. Only visible to screen readers */
                        __( 'Continue reading<span class="screen-reader-text"> "%s"</span>', 'munkysband' ),
                        array(
                          'span' => array(
                            'class' => array(),
                          ),
                        )
                      ),
                      wp_kses_post( get_the_title() )
                    )
                  );

                  // wp_link_pages(
                  //   array(
                  //     'before' => '<div class="page-links">' . esc_html__( 'Pages:', 'munkysband' ),
                  //     'after'  => '</div>',
                  //   )
                  // );
                  ?>
                </div><!-- .entry-content -->
        </div>

    </div>
  </article><!-- #post-<?php the_ID(); ?> -->

</div>
