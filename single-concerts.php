<?php
/**
 * The template for displaying all single posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package munkysband
 */

get_header();

//Affiche le descriptif complet un concert en particulier
?>

	<main id="primary" class="site-main container">

		<?php
		while ( have_posts() ) :
			the_post();

			get_template_part( 'template-parts/content', get_post_type() );

		endwhile; // End of the loop.
		?>

	</main><!-- #main -->

</div><!-- .site-info -->
</footer><!-- #colophon -->
</div><!-- #page -->

<?php get_footer(); ?>

</body>
</html>
