<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package munkysband
 */

?>


</div><!-- #page -->

 <!-- AOS Library -->
 	<!-- Ajout de AOS en statique dans la page (Avertissement "MIME" en cas d'ajout dans functions.php)-->
<script src="https://unpkg.com/aos@2.3.1/dist/aos.js"></script>

<?php wp_footer(); ?>

</body>
</html>
