<!-- Section affichant le flux de photos instagram du groupe-->
<section id="Photos" class="container-fluid section" <?php munkysband_get_parallax_background_by_slug("photos-bg"); ?>>
  <div class="row justify-content-center">
    <h2 class="col-12 text-center"
        data-aos="fade-up">
        Photos
    </h2>
    <hr class="col-4"
    data-aos="fade-up"
    data-aos-delay="200">
  </div>
      <!-- Contient le flux -->
     <div id="instafeed" class="col-12 row"></div>

     <!-- Conteneur du flux redistribué, voir instafeed.js -->
     <div class="photos-wrapper row d-flex flex-row flex-wrap justify-content-center"
      >
         <ul class="d-flex flex-column list-unstyled"
           data-aos="fade-right"
          data-aos-delay="300"
         >

         </ul>
         <ul class="d-flex flex-column list-unstyled"
         data-aos="fade-up"
        data-aos-delay="300"
        >

         </ul>
         <ul class="d-flex flex-column list-unstyled"
         data-aos="fade-left"
        data-aos-delay="300"
        >

         </ul>

     </div>

     <!-- Permet d'afficher, masquer une partie du flux (instafeed.js) -->
     <div class="row justify-content-center p-4">
       <button id="DisplayAll" class="m-2 col-4 col-lg-2" type="button" name="button">Voir plus</button>
     </div>
  </div>

</section>
