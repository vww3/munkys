<!--
Masquée de base,
s'affiche quand on clique sur une image 
-->
<div id="lightbox" class="container-fluid">
    <?php $exitUrl = get_attachment_url_by_slug("exit"); ?>
    <button type="button" name="button" class="m-3"style="background-image:url(<?php echo $exitUrl; ?>)"></button>

      <div class="justify-content-center image-wrapper pt-4">
        <div class="image-viewer">
          <img class="main-image" title="" src="" alt="">
          <img class="insta-thumbnail" title="" src="" alt="">
          <a href="">
          </a>
        </div>
      </div>
</div>
