<!--
Affiche la description du groupe avec un fond en parallax.
Pour cela, récupère le contenu d'un post de la catégorie spécifique "Description"
-->
<?php
$category = get_cat_ID('description');

$query = get_posts([
    'post_type' => 'post',
    'category'=> $category,
]);
$post = $query[0];

if($post != null):
    
 ?>
<section id="Groupe"  class="container-fluid section"<?php munkysband_get_parallax_background_by_slug('groupe-bg'); ?>>
  <article id="post-<?php echo $post->post_id; ?>" class="py-5 m-0">
	<div class="row justify-content-md-end overflow-hidden">
		<div class="entry-content col-12 col-md-6 p-4 text-md-end">
					<header class="entry-header"
          data-aos="fade-left">
						<h2 class="entry-title"
            ><?php echo $post->post_title;?></h2>
					</header><!-- .entry-header -->
					<hr
          data-aos="fade-left"
          data-aos-delay="200"
          >
          <div
            data-aos="fade-up"
            data-aos-delay="300"
          >
            <?php
  					 echo $post->post_content;
  					?>
          </div>
			</div>

	</div>

	</article>

</section>

<?php endif; ?>
