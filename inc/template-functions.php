<?php
/**
 * Functions which enhance the theme by hooking into WordPress
 *
 * @package munkysband
 */

/**
 * Adds custom classes to the array of body classes.
 *
 * @param array $classes Classes for the body element.
 * @return array
 */
function munkysband_body_classes( $classes ) {
	// Adds a class of hfeed to non-singular pages.
	if ( ! is_singular() ) {
		$classes[] = 'hfeed';
	}

	// Adds a class of no-sidebar when there is no sidebar present.
	if ( ! is_active_sidebar( 'sidebar-1' ) ) {
		$classes[] = 'no-sidebar';
	}

	return $classes;
}
add_filter( 'body_class', 'munkysband_body_classes' );

/**
 * Add a pingback url auto-discovery header for single posts, pages, or attachments.
 */
function munkysband_pingback_header() {
	if ( is_singular() && pings_open() ) {
		printf( '<link rel="pingback" href="%s">', esc_url( get_bloginfo( 'pingback_url' ) ) );
	}
}
add_action( 'wp_head', 'munkysband_pingback_header' );


//Applique à une section un fond en fonction du slug entré
function munkysband_get_background_by_slug($slug){
	$url = get_attachment_url_by_slug($slug);

	echo "style='background-image:url(".$url.")'";
}

//Retourne à une section un fond en fonction du slug entré
function munkysband_get_background_url_by_slug($slug){
	return get_attachment_url_by_slug($slug);
}

//Applique à une section un fond en parallax en fonction du slug entré
function munkysband_get_parallax_background_by_slug($slug){
		$url = get_attachment_url_by_slug($slug);
		echo 'data-parallax="scroll" data-image-src="'.$url.'"';
}

//Récupère un contenu en fonction d'un slug
function get_attachment_url_by_slug( $slug ) {
  $args = array(
    'post_type' => 'attachment',
    'name' => sanitize_title($slug),
    'posts_per_page' => 1,
    'post_status' => 'inherit',
  );
  $_header = get_posts( $args );
  $header = $_header ? array_pop($_header) : null;
  return $header ? wp_get_attachment_url($header->ID) : '';
}

//videoplayer
function munkysband_get_video_id($url){
	$splitted = explode('=',$url);
	return $splitted[1];
}

//programmation
function compareByTimeStamp($time1, $time2){
    if (strtotime($time1['date']) < strtotime($time2['date']))
        return -1;
    else if (strtotime($time1['date']) > strtotime($time2['date']))
        return 1;
    else
        return 0;
}

//Affiche un menu avec les icônes des réseaux sociaux.
function munkysband_get_socials(){

		$reseaux = wp_get_nav_menu_items('reseaux');
		//var_dump($reseaux);
		?>
	<ul>

		<?php
		foreach ($reseaux as $reseau) {
			  $reseauName = $reseau->title;

				$image = get_attachment_url_by_slug($reseauName);
				?>
				<li><a href="<?php echo $reseau->url ?>" target="_blank"><img src="<?php echo $image; ?>" alt="<?php echo $reseauName; ?>"></a></li>
				<?php

		}
	?>
	</ul><?php
}


//Récupère, filtre et trie par date la liste des Concerts issus des customs posts concerts
function munkysband_get_concerts(){

	$query = get_posts([
			'post_type' => 'concerts'
	]);

	//var_dump($query);
	$dates = array();
	$i = 0;

	foreach ($query as $concert) {
		$meta = get_post_meta($concert->ID);
		$date = str_replace("/","-",$meta['date'][0]);
		$dates[$i]['date'] = $date;
		$dates[$i]['ID'] = $concert->ID;

		$i++;
	}

	// sort array with given user-defined function
	usort($dates, "compareByTimeStamp");
	$i = 0;

	$posts = array();
	foreach ($dates as $date) {
		$ID = $date['ID'];

		$eventDate =  strtotime($date['date']);
		$currentDate = strtotime(date("d/m/Y"));

		if($eventDate >= $currentDate){
			$postQuery = get_post($ID);

			$posts[$i] = $postQuery;
			$i++;
		}

	}
	//var_dump($posts);
	return $posts;
}
