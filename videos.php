<!-- Section vidéo, affiche les différentes vidéos postées dans
les customs posts de type vidéos -->
<section id="Vidéos" class="container-fluid section"<?php munkysband_get_parallax_background_by_slug("videos-bg");  ?>>
  <div class="row justify-content-center">
    <h2 class="text-center col-12"
    data-aos="fade-up">
    Vidéos
    </h2>
    <hr class="col-4"
    data-aos="fade-up"
    data-aos-delay="200">

    <div class="videoplayer-wrapper shadow-lg col-10 col-lg-8  mb-5"
    data-aos="zoom-out-up"
    data-aos-delay="300"
    >
        <div class="row justify-content-center p-2 ">
          <!-- iframe de la vidéo -->
          <div id="video-cont">
          </div>
          <!-- Panneau de contrôle de la vidéo  -->
          <div class="py-4 videoplayer-controls col-12 row justify-content-center">
            <button class="mx-2 mx-lg-4" id="Previous" type="button" name="button"><span></span></button>
            <button class="mx-2 mx-lg-4" id="Play" type="button" name="button"><span></span></button>
            <button class="mx-2 mx-lg-4" id="Stop" type="button" name="button"><span></span></button>
            <button class="mx-2 mx-lg-4" id="Next" type="button" name="button"><span></span></button>
        </div>
        <hr class="col-8">

        <!-- Liste des vidéos -->
        <ul class="videos-wrapper pb-2 row justify-content-center">
          <?php
                // récupération des customs posts de type vidéos
                $the_query = new WP_Query(array(
          					'post_status' => 'publish',
                    'post_type'=>'videos'
          				));

          				if ( $the_query->have_posts() ) :
          					while ( $the_query->have_posts() ) :
          							$the_query->the_post();

          							get_template_part( 'template-parts/content',"videos");
          					endwhile;

          					the_posts_navigation();

          				else :

          					get_template_part( 'template-parts/content', 'none' );

          				endif;
          				?>
        </ul>
    </div>
  </div>
</div>
</section>
