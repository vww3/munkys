//Instantiation du feed instagram du compte Munkys
$(document).ready(function(){

  if($("#instafeed").length > 0){
    var feed = new Instafeed({
      accessToken: InstagramToken,
      template:'<li data-link="{{link}}" data-url="{{image}}" data-title="{{title}}" class="insta-item lb-image-item m-2"><img class="img-fluid" title="{{title}}" src="{{image}}" /></li>',
      after:SetupInstafeed,
      limit:20,
    });

    feed.run();
  }

});

var state = false;

//Après que le flux instagram est intégré dans le DOM
function SetupInstafeed(){

    //Récupère le flux instagram et le réinsert dans trois colonnes permettant l'affichage
    //de la gallerie photo selon trois colonnes "responsives"

  var i=0;

  $("#instafeed .insta-item").each(function(){
    $(this).appendTo($(".photos-wrapper .flex-column")[i]);
     i++;
     if(i > 2){
       i =0;
     }

  });


  //Bouton afficher plus - afficher moins
  $("#DisplayAll").click(function(){
         state = !state;

         if(state){
           $(".photos-wrapper").addClass("photos-expand");
            $("#DisplayAll").empty().append("Voir moins");

         }else{
           $(".photos-wrapper").removeClass("photos-expand");
            $("#DisplayAll").empty().append("Voir plus");
         }
       });
}
