$(document).ready(function(){
  wp_offset("#site-navigation");
  menu_offset("#Programmation");
  menu_offset("#Vidéos");
  menu_offset("#Photos");

  AOS.init();
});

function wp_offset(element){
  if(document.querySelector('#wpadminbar') != null){
  var adminHeight = document.querySelector('#wpadminbar').offsetHeight;

  $(element).css("padding-top", adminHeight);
}
}
//Ajoute un padding-top à chaque section correspondant à la height du menu fixed
//afin de permettre aux titres d'apparaître en dessous du menu
//quand on clique sur les ancres de celui ci
function menu_offset(element){
    if(document.querySelector('#navbar_top') != null){
    var adminHeight = document.querySelector('#navbar_top').offsetHeight;
    $(element).css("padding-top", adminHeight);
  }
}
