$(document).ready(function(){
  StartLightbox();
});


function StartLightbox(){
  console.log("StartLightbox");

  var player = document.getElementById('player');

    $(document).on("click", ".lb-video-item", function(){
    console.log("Clicked Video");
    DisplayVideoItem(this);
  });


  $(document).on( "click", ".lb-image-item", function() {
      console.log("Clicked Image");
      DisplayImageItem(this);
  });

  $(document).on( "click", ".lb-text-item", function() {
      console.log("Clicked Text");
      DisplayTextItem(this);
  });


  $("#exit").click(function() {
  console.log("Close BACK");
    DisableLightbox();
  });

  var insideViewer;

   $(".image-viewer").click(function() {
     insideViewer = true;
   });

   $(".text-viewer").click(function() {
     insideViewer = true;
   });

   $(".video-viewer").click(function() {
     insideViewer = true;
   });

  $(document).on( "click", "#lightbox", function() {
    if(insideViewer == false){
        DisableLightbox();
    }else{
      insideViewer = false;
    }
  });

}

function DisplayTextItem(textItem){

    DisableViewers();

    var title = $(textItem).attr("data-title");
    var text = $(textItem).attr("data-text");


    $(".text-viewer").empty();
    $(".text-viewer").append(title);
    $(".text-viewer").append(text);

    $(".text-viewer").css("display","inherit");

    EnableLightbox();
}

function DisplayImageItem(imageItem){

  DisableViewers();
  var url = $(imageItem).attr("data-url");
  var title= $(imageItem).attr("data-title");
  var link= $(imageItem).attr("data-link");
  //console.log(url);
   $( ".image-viewer a" ).first().attr("href", link);
   $( ".image-viewer .main-image" ).first().attr("src", url);
   $( ".image-viewer .main-image" ).first().attr("title", title);
   $( ".image-viewer .main-image" ).first().attr("alt", title);

   $(".image-viewer").css("display","inherit");

    EnableLightbox();

}

function DisplayVideoItem(videoItem){

  DisableViewers();
  console.log($(videoItem).attr("data-url"));

  var url = $(videoItem).attr("data-url");
  player.loadVideoById(url,0, "large");
  player.stopVideo();

  var title = $(videoItem).attr("data-title");
  $("#lightbox h1").empty();
  $("#lightbox h1").append(title);

  $(".video-viewer").css("display","inherit");
  EnableLightbox();
}

function EnableLightbox(){


    $("#lightbox").fadeIn("slow", function() {
      // Animation complete.
    });

}
function DisableLightbox(){

   $("#lightbox").fadeOut("slow", function() {
     player.stopVideo();
     // Animation complete.
   });
}



function DisableViewers(){

    $(".video-viewer").css("display","none");
    $(".text-viewer").css("display","none");
    $(".image-viewer").css("display","none");
}

/* YOUTUBE javascript API*/

// 2. This code loads the IFrame Player API code asynchronously.
var tag = document.createElement('script');

tag.src = "https://www.youtube.com/iframe_api";
var firstScriptTag = document.getElementsByTagName('script')[0];
firstScriptTag.parentNode.insertBefore(tag, firstScriptTag);

// 3. This function creates an <iframe> (and YouTube player)
//    after the API code downloads.
var player;
function onYouTubeIframeAPIReady() {
  player = new YT.Player('player', {
    videoId: 'PVTUls9Wm9w',
    autoplay:'false',
    events: {
      'onReady': onPlayerReady,
      'onStateChange': onPlayerStateChange
    }
  });
}

// 4. The API will call this function when the video player is ready.
function onPlayerReady(event) {
  //event.target.playVideo();
}

// 5. The API calls this function when the player's state changes.
//    The function indicates that when playing a video (state=1),
//    the player should play for six seconds and then stop.
var done = false;
function onPlayerStateChange(event) {
  /*if (event.data == YT.PlayerState.PLAYING && !done) {
    setTimeout(stopVideo, 6000);
    done = true;
  }*/
}
function stopVideo() {
  player.stopVideo();
}
