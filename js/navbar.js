//Script permettant à la barre de menu Bootstrap de se fixer en haut de l'écran
//Après avoir scrollé la première section
//Ajout d'une variation de l'opacité du menu en fonction du scroll

document.addEventListener("DOMContentLoaded", function(){

  //Mise à 0 de l'opacité de la navbar
  $('#main_nav').css('opacity', 0);

  window.addEventListener('scroll', function() {

    //variation de l'opacité en fonction du scroll
    var opacity = $('#navbar_top').css('opacity');

    if(window.scrollY/500 <= 1){
      $('#main_nav').css('opacity', window.scrollY/500);
    }else{
      $('#main_nav').css('opacity', 1);

    }

      if (window.scrollY > $("#Home").height()) {
        document.getElementById('navbar_top').classList.add('fixed-top');
        // add padding top to show content behind navbar
        navbar_height = document.querySelector('.navbar').offsetHeight;
        document.body.style.paddingTop = navbar_height + 'px';
      } else {
        document.getElementById('navbar_top').classList.remove('fixed-top');
         // remove padding top from body
        document.body.style.paddingTop = '0';
      }
  });

  var navMain = $(".navbar-collapse"); // avoid dependency on #id
    // "a:not([data-toggle])" - to avoid issues caused
    // when you have dropdown inside navbar
    navMain.on("click", "a:not([data-toggle])", null, function () {
        navMain.collapse('hide');
    });
});
