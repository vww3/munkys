
// 2. This code loads the IFrame Player API code asynchronously.
var tag = document.createElement('script');

tag.src = "https://www.youtube.com/iframe_api";
var firstScriptTag = document.getElementsByTagName('script')[0];
firstScriptTag.parentNode.insertBefore(tag, firstScriptTag);

// 3. This function creates an <iframe> (and YouTube player)
//    after the API code downloads.
var player;
var active;
var videoIndex = 0;
var list = [,];
function onYouTubeIframeAPIReady() {

  $(".videos-wrapper .card-item").each(function(index) {
    var id = $(this).attr("data-id");
      list[index] =id;
  });

  player = new YT.Player('video-cont', {
    videoId: list[videoIndex],
    autoplay:'false',
    loadPlaylist:{
            listType:'playlist',
            list:list,
            index:videoIndex,
            suggestedQuality:'small'
          },
    playerVars: {
      'autoplay': 0,
      'controls': 1,
      'loop': 1,
      'rel': 0,
      'showinfo': 0
    },
    events: {
      'onReady': onPlayerReady,
      'onStateChange': onPlayerStateChange
    },
  });
}


// 4. The API will call this function when the video player is ready.
function onPlayerReady(event) {
  //event.target.playVideo();

    UpdateActiveElement();
}

function nextVideo(){
  if(videoIndex < list.length-1){
    videoIndex++;
    pauseVideo();
    player.cueVideoById(list[videoIndex]);
    UpdateActiveElement();
  }
}

function previousVideo(){
  if(videoIndex > 0){
    videoIndex--;
    pauseVideo();
    player.cueVideoById(list[videoIndex]);
    UpdateActiveElement();
  }
}
function UpdateActiveElement(){
  $(active).removeClass("active");

  active = $(".videos-wrapper .card-item")[videoIndex];
  $(active).addClass("active");
}
// 5. The API calls this function when the player's state changes.
//    The function indicates that when playing a video (state=1),
//    the player should play for six seconds and then stop.
var done = false;
function onPlayerStateChange(event) {
  /*if (event.data == YT.PlayerState.PLAYING && !done) {
    setTimeout(stopVideo, 6000);
    done = true;
  }*/

  if(event.data == YT.PlayerState.PLAYING){
    Disable("#Play");
    Enable("#Stop");
  }else if(event.data == YT.PlayerState.PAUSED){
    Disable("#Stop");
    Enable("#Play");
  }
}
function pauseVideo() {
  player.pauseVideo();
  Disable("#Stop");
  Enable("#Play");
}

function playVideo() {
  player.playVideo();
  Disable("#Play");
  Enable("#Stop");
}

function Disable(element){

  $(element).css("display","none");

}

function Enable(element){
  $(element).css("display","inherit");
}

function selectVideo(element){
  var id = $(element).attr("data-id");
  pauseVideo();

  for (var i = 0; i < list.length; i++) {
     if(list[i] == id){
       videoIndex = i;
     }
  }

  UpdateActiveElement();

  player.cueVideoById(list[videoIndex]);
}

$(document).ready(function(){
$("#Stop").click(pauseVideo);
$("#Play").click(playVideo);
$("#Next").click(nextVideo);
$("#Previous").click(previousVideo);
$(".videos-wrapper .card-item").click(function(){
  selectVideo(this);
});
Disable("#Stop");
});
